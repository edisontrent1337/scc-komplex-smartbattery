/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.smartbattery.internal;

import javax.measure.quantity.Energy;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.jdt.annotation.Nullable;
import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.thing.ChannelUID;
import org.eclipse.smarthome.core.thing.Thing;
import org.eclipse.smarthome.core.thing.ThingStatus;
import org.eclipse.smarthome.core.thing.binding.BaseThingHandler;
import org.eclipse.smarthome.core.types.Command;
import org.eclipse.smarthome.core.types.State;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link SmartBatteryHandler} is responsible for handling commands, which are
 * sent to one of the channels.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
@NonNullByDefault
public class SmartBatteryHandler extends BaseThingHandler {

    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(SmartBatteryHandler.class);
    @Nullable
    private SmartBatteryConfiguration config;

    private SmartBattery smartBattery;

    public SmartBatteryHandler(Thing thing) {
        super(thing);
        this.smartBattery = new SmartBattery(this);
    }

    @Override
    public void handleCommand(ChannelUID channelUID, Command command) {
        switch (channelUID.getIdWithoutGroup()) {
            case SmartBatteryBindingConstants.WORK_IN:
                if (command instanceof QuantityType<?>) {
                    smartBattery.chargeAmount((QuantityType<Energy>) command);
                }
                break;
        }
    }

    @Override
    public void initialize() {
        // logger.debug("Start initializing!");
        config = getConfigAs(SmartBatteryConfiguration.class);
        smartBattery.initialize(config.capacityKwh);
        // TODO: Initialize the handler.
        // The framework requires you to return from this method quickly. Also, before leaving this method a thing
        // status from one of ONLINE, OFFLINE or UNKNOWN must be set. This might already be the real thing status in
        // case you can decide it directly.
        // In case you can not decide the thing status directly (e.g. for long running connection handshake using WAN
        // access or similar) you should set status UNKNOWN here and then decide the real status asynchronously in the
        // background.

        // set the thing status to UNKNOWN temporarily and let the background task decide for the real status.
        // the framework is then able to reuse the resources from the thing handler initialization.
        // we set this upfront to reliably check status updates in unit tests.
        updateStatus(ThingStatus.UNKNOWN);

        // Example for background initialization:
        scheduler.execute(() -> {
            boolean thingReachable = true; // <background task with long running initialization here>
            // when done do:
            if (thingReachable) {
                updateStatus(ThingStatus.ONLINE);
            } else {
                updateStatus(ThingStatus.OFFLINE);
            }
        });

        // logger.debug("Finished initializing!");

        // Note: When initialization can NOT be done set the status with more details for further
        // analysis. See also class ThingStatusDetail for all available status details.
        // Add a description to give user information to understand why thing does not work as expected. E.g.
        // updateStatus(ThingStatus.OFFLINE, ThingStatusDetail.CONFIGURATION_ERROR,
        // "Can not access device as username and/or password are invalid");
    }

    public void postUpdate(String channel, State state) {
        updateState(channel, state);
    }

    @Override
    public void channelLinked(ChannelUID channelUid) {
        switch (channelUid.getIdWithoutGroup()) {
            case SmartBatteryBindingConstants.REMAINING_CAPACITY_KWH:
                postUpdate(SmartBatteryBindingConstants.REMAINING_CAPACITY_KWH, smartBattery.getRemainingCapacity());
                break;

            case SmartBatteryBindingConstants.STATE_OF_CHARGE:
                postUpdate(SmartBatteryBindingConstants.STATE_OF_CHARGE, smartBattery.getStateOfCharge());
                break;
        }
    }
}
