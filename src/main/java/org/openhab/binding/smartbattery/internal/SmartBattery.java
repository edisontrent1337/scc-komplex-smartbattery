/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.smartbattery.internal;

import javax.measure.quantity.Dimensionless;
import javax.measure.quantity.Energy;

import org.eclipse.smarthome.core.library.types.QuantityType;
import org.eclipse.smarthome.core.library.unit.SmartHomeUnits;

/**
 * The {@link SmartBattery} represents a battery that can be charged and drained.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
public class SmartBattery {
    private static final QuantityType<Energy> ZERO_CAPACITY = new QuantityType<Energy>(0, SmartHomeUnits.KILOWATT_HOUR);

    private SmartBatteryHandler smartBatteryHandler;
    private QuantityType<Energy> maxCapacity;

    private QuantityType<Energy> remainingCapacity;
    private QuantityType<Dimensionless> stateOfCharge;

    public SmartBattery(SmartBatteryHandler smartBatteryHandler) {
        this.smartBatteryHandler = smartBatteryHandler;
    }

    public void chargeAmount(QuantityType<Energy> amount) {
        remainingCapacity = remainingCapacity.add(amount);
        if (remainingCapacity.compareTo(maxCapacity) > 0) {
            remainingCapacity = maxCapacity;
        } else if (remainingCapacity.compareTo(ZERO_CAPACITY) < 0) {
            remainingCapacity = ZERO_CAPACITY;
        }

        stateOfCharge = new QuantityType<Dimensionless>(
                100 * remainingCapacity.doubleValue() / maxCapacity.doubleValue(), SmartHomeUnits.PERCENT);
        updateHandler();
    }

    private void updateHandler() {
        smartBatteryHandler.postUpdate(SmartBatteryBindingConstants.REMAINING_CAPACITY_KWH, remainingCapacity);
        smartBatteryHandler.postUpdate(SmartBatteryBindingConstants.STATE_OF_CHARGE, stateOfCharge);
    }

    public void initialize(double initialCapacity) {
        this.stateOfCharge = new QuantityType<Dimensionless>(100, SmartHomeUnits.PERCENT);
        this.maxCapacity = new QuantityType<Energy>(initialCapacity, SmartHomeUnits.KILOWATT_HOUR);
        this.remainingCapacity = new QuantityType<Energy>(initialCapacity, SmartHomeUnits.KILOWATT_HOUR);

        updateHandler();
    }

    public QuantityType<Energy> getRemainingCapacity() {
        return remainingCapacity;
    }

    public QuantityType<Dimensionless> getStateOfCharge() {
        return stateOfCharge;
    }
}
