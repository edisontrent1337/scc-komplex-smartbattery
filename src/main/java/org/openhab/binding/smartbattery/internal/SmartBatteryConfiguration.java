/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.smartbattery.internal;

/**
 * The {@link SmartBatteryConfiguration} class contains fields mapping thing configuration parameters.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
public class SmartBatteryConfiguration {

    /**
     * Sample configuration parameter. Replace with your own.
     */
    public String config1;
    public int capacityKwh;
}
