/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.smartbattery.internal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 * The {@link SmartBatteryBindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
@NonNullByDefault
public class SmartBatteryBindingConstants {

    private static final String BINDING_ID = "smartbattery";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_SAMPLE = new ThingTypeUID(BINDING_ID, "battery");

    // List of all Channel ids
    public static final String STATE_OF_CHARGE = "stateOfCharge";
    public static final String REMAINING_CAPACITY_KWH = "remainingCapacityKwh";
    public static final String WORK_IN = "workIn";
    public static final String WORK_OUT = "workOut";

}
